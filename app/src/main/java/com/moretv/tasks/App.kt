package com.moretv.tasks

import android.app.Application
import com.moretv.tasks.di.commonModule
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(listOf(commonModule))
        }
    }
}