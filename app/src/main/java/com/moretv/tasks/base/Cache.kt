package com.moretv.tasks.base

interface Cache<K, V> {
    fun put(key: K, value: V)
    fun get(key: K): V?
    fun existKey(key: K): Boolean
}