package com.moretv.tasks.base

import java.util.*
import kotlin.collections.LinkedHashMap

class RuntimeCache<K, V> : Cache<K, V> {

    private val cache = Collections.synchronizedMap<K, V>(LinkedHashMap<K, V>())

    override fun put(key: K, value: V) {
        cache[key] = value
    }

    override fun get(key: K): V? = cache[key]

    override fun existKey(key: K): Boolean = cache.containsKey(key)

}