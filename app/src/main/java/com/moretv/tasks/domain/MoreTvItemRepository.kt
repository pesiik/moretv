package com.moretv.tasks.domain

import com.moretv.tasks.model.MoreTvItem

interface MoreTvItemRepository {
    fun getItems(): List<MoreTvItem>
}