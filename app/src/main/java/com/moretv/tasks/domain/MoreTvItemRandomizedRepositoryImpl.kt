package com.moretv.tasks.domain

import com.moretv.tasks.model.Event
import com.moretv.tasks.model.MoreTvItem
import com.moretv.tasks.model.Move
import com.moretv.tasks.model.Notice
import com.moretv.tasks.storage.MoreTvItemStorage
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class MoreTvItemRandomizedRepositoryImpl(
    private val moreTvItemStorage: MoreTvItemStorage
) : MoreTvItemRepository {

    override fun getItems(): List<MoreTvItem> {
        val cacheItems = moreTvItemStorage.getItems()
        return if (cacheItems.isNotEmpty()) {
            cacheItems
        } else {
            val itemCount = Random.nextInt(10, 100)
            List(itemCount, ::randomType).apply {
                moreTvItemStorage.setItems(this)
            }
        }
    }

    private fun randomType(position: Int): MoreTvItem {
        val now = Date().time
        val yesterday = now - TimeUnit.DAYS.toMillis(1)
        return when (Random.nextInt(0, 3)) {
            0 -> Notice(
                id = position,
                flightDate = Date(Random.nextLong(yesterday, now)),
                gate = "Gate №${Random.nextInt(1, 10)}",
                name = "Notice №$position"
            )
            1 -> {
                val startTime = Date(Random.nextLong(yesterday, now))
                val description = StringBuilder().apply {
                    append("Event description")
                    for (i in 1..Random.nextInt(30, 2000)) {
                        append(" bla")
                    }
                }
                Event(
                    id = position,
                    startTime = startTime,
                    endTime = Date(
                        Random.nextLong(yesterday, now).coerceAtLeast(
                            minimumValue = startTime.time + 1000
                        )
                    ),
                    name = "Event ${Random.nextInt(1, 10)}",
                    description = description.toString()
                )
            }
            2 -> {
                val description = StringBuilder().apply {
                    append("Move description")
                    for (i in 0..Random.nextInt(3, 20)) {
                        append(" bla")
                    }
                }
                Move(
                    id = position,
                    fromPlace = "From place №${Random.nextInt(1, 10)}",
                    toPlace = "To place №${Random.nextInt(1, 10)}",
                    estimateTime = Random.nextDouble(1.0, 24.0),
                    name = "Move №$position",
                    description = description.toString()
                )
            }
            else -> throw IllegalStateException()
        }
    }
}