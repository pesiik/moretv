package com.moretv.tasks.domain

class MoreTvItemInteractor(private val moreTvItemRepository: MoreTvItemRepository) {
    fun getItems() = moreTvItemRepository.getItems()
    fun getItem(id: Int) = getItems().first { it.id == id }
}