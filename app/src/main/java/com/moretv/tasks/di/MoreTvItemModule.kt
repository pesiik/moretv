package com.moretv.tasks.di

import com.moretv.tasks.domain.MoreTvItemInteractor
import com.moretv.tasks.domain.MoreTvItemRandomizedRepositoryImpl
import com.moretv.tasks.domain.MoreTvItemRepository
import com.moretv.tasks.presentation.DetailItemViewModel
import com.moretv.tasks.presentation.ItemListViewModel
import com.moretv.tasks.storage.MoreTvItemStorage
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val moreTvItemModule = module {
    viewModel { ItemListViewModel(moreTvItemInteractor = get()) }
    viewModel { DetailItemViewModel(moreTvItemInteractor = get()) }
    factory { MoreTvItemInteractor(moreTvItemRepository = get()) }
    factory<MoreTvItemRepository> {
        MoreTvItemRandomizedRepositoryImpl(moreTvItemStorage = get())
    }
    factory { MoreTvItemStorage(cache = get()) }
}