package com.moretv.tasks.di

import com.moretv.tasks.base.Cache
import com.moretv.tasks.base.RuntimeCache
import org.koin.dsl.module

val commonModule = module {
    single<Cache<String, Any>> { RuntimeCache<String, Any>() }
}