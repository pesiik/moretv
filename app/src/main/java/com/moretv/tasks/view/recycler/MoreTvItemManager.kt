package com.moretv.tasks.view.recycler

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.moretv.tasks.R
import com.moretv.tasks.model.Event
import com.moretv.tasks.model.MoreTvItem
import com.moretv.tasks.model.Move
import com.moretv.tasks.model.Notice
import java.text.SimpleDateFormat
import java.util.*

class MoreTvItemManager {

    fun getViewHolder(parent: ViewGroup, viewType: Int): UniversalViewHolder {
        return UniversalViewHolder(parent, getMoreTvItemLayout(viewType))
    }

    fun getSameChecking(oldMoreTvItem: MoreTvItem, newMoreTvItem: MoreTvItem): Boolean {
        return when (oldMoreTvItem) {
            is Event -> {
                if (newMoreTvItem is Event) {
                    oldMoreTvItem.startTime == newMoreTvItem.startTime &&
                            oldMoreTvItem.endTime == newMoreTvItem.endTime &&
                            oldMoreTvItem.description == newMoreTvItem.description &&
                            oldMoreTvItem.name == newMoreTvItem.name
                } else {
                    false
                }
            }
            is Move -> {
                if (newMoreTvItem is Move) {
                    oldMoreTvItem.fromPlace == newMoreTvItem.fromPlace &&
                            oldMoreTvItem.toPlace == newMoreTvItem.toPlace &&
                            oldMoreTvItem.description == newMoreTvItem.description &&
                            oldMoreTvItem.name == newMoreTvItem.name
                } else {
                    false
                }
            }
            is Notice -> {
                if (newMoreTvItem is Notice) {
                    oldMoreTvItem.flightDate == newMoreTvItem.flightDate &&
                            oldMoreTvItem.gate == newMoreTvItem.gate &&
                            oldMoreTvItem.name == newMoreTvItem.name
                } else {
                    false
                }
            }
            else -> throw IllegalStateException("This kind of predicate doesn't support in MoreTvItemManager")
        }
    }

    fun getViewType(moreTvItem: MoreTvItem): Int {
        return when (moreTvItem) {
            is Event -> EVENT_VIEW_TYPE
            is Move -> MOVE_VIEW_TYPE
            is Notice -> NOTICE_VIEW_TYPE
            else -> throw IllegalStateException("This kind of MoreTvItem doesn't support in MoreTvItemManager")
        }
    }


    @LayoutRes
    private fun getMoreTvItemLayout(viewType: Int): Int {
        return when (viewType) {
            EVENT_VIEW_TYPE -> R.layout.more_tv_item
            MOVE_VIEW_TYPE -> R.layout.more_tv_item
            NOTICE_VIEW_TYPE -> R.layout.notice_item
            else -> throw IllegalStateException("This kind of MoreTvItem doesn't support in MoreTvItemManager")
        }
    }

    companion object {
        private const val EVENT_VIEW_TYPE = 0
        private const val MOVE_VIEW_TYPE = 1
        private const val NOTICE_VIEW_TYPE = 2
    }
}

class UniversalViewHolder(
    parent: ViewGroup,
    @LayoutRes res: Int,
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(res, parent, false)
) {
    fun onBind(moreTvItem: MoreTvItem, onMoreTvItemClick: OnMoreTvItemClick) {
        val action = when (moreTvItem) {
            is Event -> EventItemDelegate(moreTvItem)
            is Move -> MoveItemDelegate(moreTvItem)
            is Notice -> NoticeItemDelegate(moreTvItem)
            else -> throw IllegalStateException("This kind of delegate doesn't support in MoreTvItemManager")
        }
        action.invoke(itemView)
        itemView.setOnClickListener {
            onMoreTvItemClick.invoke(moreTvItem)
        }
    }
}

private val format = SimpleDateFormat("dd hh:mm", Locale.US)

class EventItemDelegate(private val event: Event) : MoreTvItemDelegate {
    override fun invoke(view: View) {
        val title = view.findViewById<TextView>(R.id.title)
        val description = view.findViewById<TextView>(R.id.description)
        val additionalInformation = view.findViewById<TextView>(R.id.additional_information)
        title.text = event.name
        description.text = event.description
        additionalInformation.text = view.context.resources.getString(
            R.string.divided_sentences_pattern,
            format.format(event.startTime ?: Date()),
            format.format(event.endTime ?: Date())
        )
    }
}

class MoveItemDelegate(private val move: Move) : MoreTvItemDelegate {
    override fun invoke(view: View) {
        val title = view.findViewById<TextView>(R.id.title)
        val description = view.findViewById<TextView>(R.id.description)
        val additionalInformation = view.findViewById<TextView>(R.id.additional_information)
        title.text = move.name
        description.text = move.description
        additionalInformation.text = view.context.resources.getString(
            R.string.divided_sentences_pattern,
            move.fromPlace,
            move.toPlace
        )
    }
}

class NoticeItemDelegate(private val notice: Notice) : MoreTvItemDelegate {
    override fun invoke(view: View) {
        val gate = view.findViewById<TextView>(R.id.gate)
        val flightDate = view.findViewById<TextView>(R.id.flightDate)
        gate.text = notice.gate
        flightDate.text = format.format(notice.flightDate ?: Date())
    }
}
typealias MoreTvItemDelegate = (View) -> Unit