package com.moretv.tasks.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.moretv.tasks.R
import com.moretv.tasks.model.Event
import com.moretv.tasks.model.MoreTvItem
import com.moretv.tasks.model.Move
import com.moretv.tasks.model.Notice
import com.moretv.tasks.presentation.DetailItemViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

class DetailItemFragment : Fragment() {

    private val args: DetailItemFragmentArgs by navArgs()
    private val viewModel: DetailItemViewModel by viewModel()
    private val format = SimpleDateFormat("dd hh:mm", Locale.US)

    private lateinit var title: TextView
    private lateinit var additionalInfo: TextView
    private lateinit var description: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        viewModel.item.observe(viewLifecycleOwner, Observer(::updateItem))
        viewModel.getItem(args.itemId)
    }

    private fun initView() {
        title = requireView().findViewById(R.id.title)
        additionalInfo = requireView().findViewById(R.id.additional_information)
        description = requireView().findViewById(R.id.description)
    }

    private fun updateItem(moreTvItem: MoreTvItem) {
        when (moreTvItem) {
            is Move -> {
                title.text = moreTvItem.name
                additionalInfo.text = resources.getString(
                    R.string.divided_sentences_pattern,
                    moreTvItem.fromPlace,
                    moreTvItem.toPlace
                )
                additionalInfo.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        android.R.color.holo_blue_light
                    )
                )
                description.text = moreTvItem.description
                description.isVisible = true
            }
            is Event -> {
                title.text = moreTvItem.name
                additionalInfo.text = resources.getString(
                    R.string.divided_sentences_pattern,
                    format.format(moreTvItem.startTime ?: Date()),
                    format.format(moreTvItem.endTime ?: Date())
                )
                additionalInfo.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        android.R.color.holo_green_dark
                    )
                )
                description.text = moreTvItem.description
                description.isVisible = true
            }
            is Notice -> {
                title.text = moreTvItem.gate
                additionalInfo.setTextColor(
                    ContextCompat.getColor(
                        requireContext(),
                        android.R.color.holo_red_light
                    )
                )
                additionalInfo.text = format.format(moreTvItem.flightDate ?: Date())
                description.isVisible = false
            }
        }
    }
}