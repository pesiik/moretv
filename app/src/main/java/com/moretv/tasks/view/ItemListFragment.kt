package com.moretv.tasks.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.moretv.tasks.NavGraphDirections
import com.moretv.tasks.R
import com.moretv.tasks.di.moreTvItemModule
import com.moretv.tasks.model.MoreTvItem
import com.moretv.tasks.presentation.ItemListViewModel
import com.moretv.tasks.utils.bindToLifecycle
import com.moretv.tasks.view.recycler.MoreTvItemsAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class ItemListFragment : Fragment() {

    private val viewModel: ItemListViewModel by viewModel()

    private lateinit var moreTvItems: RecyclerView
    private lateinit var adapter: MoreTvItemsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindToLifecycle(moreTvItemModule)
        initViews()
        viewModel.items.observe(viewLifecycleOwner, Observer(::updateItems))
        viewModel.updateMoreTvItems()
    }

    private fun initViews() {
        moreTvItems = requireView().findViewById(R.id.moreTvItems)
        val divider = DividerItemDecoration(
            requireContext(),
            (moreTvItems.layoutManager as LinearLayoutManager).orientation
        )
        moreTvItems.addItemDecoration(divider)
        adapter = MoreTvItemsAdapter { moreTvItem ->
            findNavController().navigate(
                NavGraphDirections.actionItemListToDetail(moreTvItem.id)
            )
        }
        moreTvItems.adapter = adapter
    }

    private fun updateItems(items: List<MoreTvItem>) {
        adapter.updateItems(items)
    }
}