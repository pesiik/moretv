package com.moretv.tasks.view.recycler

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.moretv.tasks.model.MoreTvItem

class MoreTvItemsAdapter(
    private val onMoreTvItemClick: OnMoreTvItemClick
) : RecyclerView.Adapter<UniversalViewHolder>() {

    private var items: MutableList<MoreTvItem> = mutableListOf()

    private val manager = MoreTvItemManager()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UniversalViewHolder {
        return manager.getViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: UniversalViewHolder, position: Int) {
        holder.onBind(items[position], onMoreTvItemClick)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return manager.getViewType(items[position])
    }

    fun updateItems(newList: List<MoreTvItem>) {
        val diffUtilCallback = DiffUtil.calculateDiff(
            DiffUtilCallback(oldList = items, newList = newList),
            true
        )
        items.clear()
        items.addAll(newList)
        diffUtilCallback.dispatchUpdatesTo(this)
    }

    private inner class DiffUtilCallback(
        private val oldList: List<MoreTvItem>,
        private val newList: List<MoreTvItem>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return manager.getSameChecking(oldList[oldItemPosition], newList[newItemPosition])
        }
    }
}
typealias OnMoreTvItemClick = (MoreTvItem) -> Unit