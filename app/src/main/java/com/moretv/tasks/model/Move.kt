package com.moretv.tasks.model

data class Move(
    var fromPlace: String? = null,
    var toPlace: String? = null,
    var estimateTime: Double? = null,
    val description: String,
    override val name: String,
    override val id: Int
) : MoreTvItem()