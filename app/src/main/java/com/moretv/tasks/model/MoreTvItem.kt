package com.moretv.tasks.model

/**
 * Маркер типов для домейн-сущностей, которые будут конвертированы в данные для отображения в списках
 */
abstract class MoreTvItem {
    abstract val id: Int
    abstract val name: String?
}