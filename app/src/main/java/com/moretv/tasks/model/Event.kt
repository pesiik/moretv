package com.moretv.tasks.model

import java.util.*

data class Event(
    var startTime: Date? = null,
    var endTime: Date? = null,
    val description: String,
    override val name: String,
    override val id: Int
) : MoreTvItem()