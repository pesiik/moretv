package com.moretv.tasks.model

import java.util.*

data class Notice(
    val flightDate: Date? = null,
    val gate: String? = null,
    override val name: String,
    override val id: Int
) : MoreTvItem()