package com.moretv.tasks.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.moretv.tasks.domain.MoreTvItemInteractor
import com.moretv.tasks.model.MoreTvItem

class DetailItemViewModel(private val moreTvItemInteractor: MoreTvItemInteractor) : ViewModel() {

    private val _item: MutableLiveData<MoreTvItem> = MutableLiveData()
    val item: LiveData<MoreTvItem>
        get() = _item

    fun getItem(itemId: Int) {
        val result = moreTvItemInteractor.getItem(itemId)
        _item.postValue(result)
    }

}