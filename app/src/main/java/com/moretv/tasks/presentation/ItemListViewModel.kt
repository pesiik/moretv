package com.moretv.tasks.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.moretv.tasks.domain.MoreTvItemInteractor
import com.moretv.tasks.model.MoreTvItem

class ItemListViewModel(private val moreTvItemInteractor: MoreTvItemInteractor) : ViewModel() {

    private val _items: MutableLiveData<List<MoreTvItem>> = MutableLiveData()
    val items: LiveData<List<MoreTvItem>>
        get() = _items

    fun updateMoreTvItems() {
        val result = moreTvItemInteractor.getItems()
        _items.postValue(result)
    }

}