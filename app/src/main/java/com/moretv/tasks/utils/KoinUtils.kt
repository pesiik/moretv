package com.moretv.tasks.utils

import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.core.module.Module

/**
 * Пытается загрузить koin модули в момент вызова и выгружает в конце жизненного цикла фрагмента
 */
fun Fragment.bindToLifecycle(vararg modules: Module) {
    modules.filter { !ModuleUtils.isLoaded(it) }.let { newModules ->
        loadKoinModules(newModules)

        lifecycle.addObserver(object : DefaultLifecycleObserver {

            override fun onDestroy(owner: LifecycleOwner) {
                unloadKoinModules(newModules)
                lifecycle.removeObserver(this)
            }
        })
    }
}