package com.moretv.tasks.utils;

import org.koin.core.module.Module;

public class ModuleUtils {

    @SuppressWarnings("KotlinInternalInJava")
    static boolean isLoaded(Module module) {
        return module.isLoaded();
    }
}
