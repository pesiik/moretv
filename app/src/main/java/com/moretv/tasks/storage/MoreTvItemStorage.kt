package com.moretv.tasks.storage

import com.moretv.tasks.base.Cache
import com.moretv.tasks.model.MoreTvItem

class MoreTvItemStorage(private val cache: Cache<String, Any>) {

    private val tag = "MoreTvItemStorage"
    private val moreTvItemsKey = "${tag}_more_tv_items_key"

    @Suppress("UNCHECKED_CAST")
    fun getItems(): List<MoreTvItem> {
        return if (cache.existKey(moreTvItemsKey)) {
            try {
                cache.get(moreTvItemsKey) as List<MoreTvItem>
            } catch (e: Exception) {
                emptyList()
            }
        } else {
            emptyList()
        }
    }

    fun setItems(items: List<MoreTvItem>) {
        cache.put(moreTvItemsKey, items)
    }
}